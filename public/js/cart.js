var counterCart = document.querySelector('.counter-cart');
var tbodyCart = document.querySelector('.tbody-cart');
var buttonPayment = document.querySelector('.buttonPayment');
var closePayment = document.querySelector('.closePayment');
var tbodyCartModal = document.querySelector('.tbody-cart-modal');
var aLength = 0;
var totalTotal = 0;
//console.log(localStorage);
for (var i = 0; i<localStorage.length; i++) {
    aLength += Number(JSON.parse(localStorage.getItem(localStorage.key(i))).counter);
}
counterCart.textContent = aLength;
if(localStorage.length > 0) {
    counterCart.classList.add('have-items');
} else {
    counterCart.classList.remove('have-items');
}

buttonPayment.addEventListener('click', (event)=>{
    event.preventDefault();
    document.querySelector(".modal").classList.add("show");
    document.querySelector(".modal").style.display = "block";
    document.querySelector(".modal").style.backgroundColor  = "rgba(0,0,0,.4)";

    for (var i = 0; i<localStorage.length; i++) {
        var product = JSON.parse(localStorage.getItem(localStorage.key(i)));
        //console.log(product);
        var nameProduct = product.name;
        var billingProduct = product.billing;
        var imageProduct = product.image;
        let newRow = tbodyCartModal.insertRow(tbodyCartModal.rows.length);
        let total = billingProduct * product.counter;
       

        newRow.innerHTML = '<tr>' +
            '<td>'+(i+1)+'</td>' +
            '<td> '+product.counter+'</td>' +
            '<td>'+nameProduct+'</td>' +
            '<td>$'+total+'</td>' +
            '</tr>';
    }

    let newRow = tbodyCartModal.insertRow(tbodyCartModal.rows.length);
    newRow.innerHTML = '<tr>' +
        '<td></td>' +
        '<td></td>' +
        '<td></td>' +
        '<td><strong>$' + totalTotal + '</strong></td>' +
        '</tr>';

});

closePayment.addEventListener('click', (event)=>{
    document.querySelector(".modal").classList.add("hide");
    document.querySelector(".modal").style.display = "none";
});

function modifyCart(key, flag) {
    var product = JSON.parse(localStorage.getItem(key));
    var arrayProduct = {};
    if(!product){
        //console.log('entro en true');
        arrayProduct = {
            counter: 1,
            name: elementButtonAddCart.dataset.id,
            billing: elementButtonAddCart.dataset.billing,
            image: elementButtonAddCart.dataset.image
        };
    } else {
        if(!flag){
            if((Number(product.counter) - 1) <= 0){
                localStorage.removeItem(key);
                let sumCart = 0;
                for (var i = 0; i<localStorage.length; i++) {
                    sumCart += Number(JSON.parse(localStorage.getItem(localStorage.key(i))).counter);
                }
                counterCart.textContent = sumCart;
                totalTotal = 0;
                printCartTable();
                printCartTotal();
                return false;
            } else {
                arrayProduct = {
                    counter: Number(product.counter) - 1,
                    name: product.name,
                    billing: product.billing,
                    image: product.image
                };
            }
        } else {
            arrayProduct = {
                counter: Number(product.counter) + 1,
                name: product.name,
                billing: product.billing,
                image: product.image
            };
        }
    }
    var sumCart = 0;
    localStorage.setItem(key, JSON.stringify(arrayProduct));
    counterCart.classList.add('have-items');
    for (var i = 0; i<localStorage.length; i++) {
        sumCart += Number(JSON.parse(localStorage.getItem(localStorage.key(i))).counter);
    }
    if(sumCart <= 0){
        counterCart.classList.remove('have-items');
    }
    counterCart.textContent = sumCart;
    console.log('localStorage', localStorage);
    printCartTable();
    printCartTotal();
}

let printCartTable = () => {
    var rowCount = tbodyCart.rows.length-1;
    for (var i = rowCount; i >= 0 ; i--) {
        tbodyCart.deleteRow(i);
    }
    totalTotal = 0;
    for (var i = 0; i<localStorage.length; i++) {
        var product = JSON.parse(localStorage.getItem(localStorage.key(i)));
        //console.log(product);
        var nameProduct = product.name;
        var billingProduct = product.billing;
        var imageProduct = product.image;
        let newRow = tbodyCart.insertRow(tbodyCart.rows.length);
        let total = billingProduct * product.counter;
        totalTotal += total;

        newRow.innerHTML = '<tr>' +
            '<td>'+(i+1)+'</td>' +
            '<td><a data-id="'+ localStorage.key(i) +'" href="#" class="plus-add-cart"><i class="fal fa-plus-circle"></i></a><a data-id="'+ localStorage.key(i) +'" class="minus-add-cart" href="#"> <i class="fal fa-minus-circle"></i></a>  '+product.counter+'</td>' +
            '<td><img width="50" src="'+ imageProduct +'">'+nameProduct+'</td>' +
            '<td>$'+total+'</td>' +
            '</tr>';
    }
    var aPlus = document.querySelectorAll('.plus-add-cart');
    aPlus.forEach((aElement) => {
        aElement.addEventListener('click', (event) => {
            event.preventDefault();
            console.log('plus')
            console.log(aElement.dataset.id)
            modifyCart(aElement.dataset.id, true);
        });
    });
    var aMinus = document.querySelectorAll('.minus-add-cart');
    aMinus.forEach((aElement) => {
        aElement.addEventListener('click', (event) => {
            event.preventDefault();
            console.log('minus')
            modifyCart(aElement.dataset.id, false);
        });
    });
};
let printCartTotal = () => {
    let newRow = tbodyCart.insertRow(tbodyCart.rows.length);
    newRow.innerHTML = '<tr>' +
        '<td></td>' +
        '<td></td>' +
        '<td></td>' +
        '<td><strong>$' + totalTotal + '</strong></td>' +
        '</tr>';
};

printCartTable();
printCartTotal();


