<?php

use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $user = User::find(1);
    Auth::login($user, true);
    return view('index');
})->name('index');
Route::get('/cart', function () {
    $user = Auth::user();
    return view('cart', [
        'user'=>$user,
        'intent' => $user->createSetupIntent()
    ]);
});

Route::post('/subscription', 'App\Http\Controllers\SubscriptionController@processSubscription')->name('subscription.create');
