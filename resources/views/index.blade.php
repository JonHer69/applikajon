<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>

    <!-- Basic Page Needs
    ================================================== -->
    <meta charset="utf-8">
    <title>Prueba Applika Jon</title>

    <!-- Mobile Specific Metas
    ================================================== -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="Bootstrap App Landing Template">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=5.0">

    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="images/favicon.png" />

    <!-- PLUGINS CSS STYLE -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>

    <!-- CUSTOM CSS -->
    <link href="{{asset('css/styles.css')}}" rel="stylesheet">

</head>

<body class="body-wrapper" data-spy="scroll" data-target=".privacy-nav">


<nav class="navbar main-nav navbar-expand-lg px-2 px-sm-0 py-2 py-lg-0">
    <div class="container">
        <a class="navbar-brand" href="{{url('/')}}">Jon-Logo</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="ti-menu"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item dropdown active">
                    <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown">Home
                        <span><i class="ti-angle-down"></i></span>
                    </a>
                    <!-- Dropdown list -->
                    <ul class="dropdown-menu">
                        <li><a class="dropdown-item active" href="{{url('/')}}">Homepage</a></li>
                    </ul>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="{{url('/cart')}}" data-toggle="dropdown"><i class="fas fa-cart-arrow-down"></i> Cart  <span class="counter-cart">0</span>
                        <span><i class="ti-angle-down"></i></span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</nav>

<section class="section gradient-banner">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-md-6 order-2 order-md-1 text-center text-md-left">
                <h1 class="text-white font-weight-bold mb-4">Prove your worth with a game controller</h1>
                <p class="text-white mb-5">Jon's game controllers are for serious gamers like you.</p>
                <a value="key1" href="{{url('/')}}" class="btn btn-sm btn-primary">Try Now</a>
            </div>
            <div class="col-md-6 text-center order-1 order-md-2">
                <img class="img-fluid" src="{{asset('images/control.png')}}" alt="screenshot">
            </div>
        </div>
    </div>
</section>
<!--====  End of Hero Section  ====-->

<section class="section pt-0 position-relative pull-top">
    <div class="container">
        <div class="rounded shadow p-5 bg-white">
            <div class="row">
            @if ($message = Session::get('success'))
                    <div class="alert alert-success alert-block">
                      <button type="button" class="close" data-dismiss="alert">×</button>
                            <strong>{{ $message }}</strong>
                    </div>
                    @endif
                <div class="col-lg-4 col-md-6 mt-5 mt-md-0 text-center">
                    <img src="{{asset('images/control.png')}}" width="120">
                    <h3 class="mt-4 text-capitalize h5 ">Xbox Series X Controller</h3>
                    <p><b>$1600</b></p>
                    <a href="{{url('/')}}" class="btn btn-sm btn-primary">Buy Now</a>
                    <a data-image="{{asset('images/control.png')}}" data-id="Xbox Series X Controller" data-billing="1600" data-value="price_1KPvDNJfnwFYLDV8Ykfh5qr4" href="{{url('/')}}" class="btn btn-sm btn-warning add-cart">Add to Cart</a>
                </div>
                <div class="col-lg-4 col-md-6 mt-5 mt-md-0 text-center">
                    <img src="{{asset('images/ps4.png')}}" width="120">
                    <h3 class="mt-4 text-capitalize h5 ">Play Station 5 Controller</h3>
                    <p><b>$1800</b></p>
                    <a href="{{url('/')}}" class="btn btn-sm btn-primary">Buy Now</a>
                    <a data-image="{{asset('images/ps4.png')}}" data-id="Play Station 5 Controller" data-billing="1800" data-value="price_1KPvE1JfnwFYLDV8yLQAL1md" href="{{url('/')}}" class="btn btn-sm btn-warning add-cart">Add to Cart</a>
                </div>
                <div class="col-lg-4 col-md-12 mt-5 mt-lg-0 text-center">
                    <img src="{{asset('images/joycons.png')}}" width="120">
                    <h3 class="mt-4 text-capitalize h5 ">Switch Joy-Con</h3>
                    <p><b>$2200</b></p>
                    <a href="{{url('/')}}" class="btn btn-sm btn-primary">Buy Now</a>
                    <a data-image="{{asset('images/joycons.png')}}" data-id="Switch Joy-Con" data-billing="1800" data-value="price_1KPvEkJfnwFYLDV8B4kP39xE" href="{{url('/')}}" class="btn btn-sm btn-warning add-cart">Add to Cart</a>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="call-to-action-app section bg-blue">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2>It's time to change your mind</h2>
                <p>Video game consoles are more popular than ever...  you already know which brand you'll choose?</p>
                <ul class="list-inline">
                    <li class="list-inline-item">
                        <a href="" class="btn btn-rounded-icon">
                            <i class="ti-apple"></i>
                            Xbox
                        </a>
                    </li>
                    <li class="list-inline-item">
                        <a href="" class="btn btn-rounded-icon">
                            <i class="ti-android"></i>
                            Play Station
                        </a>
                    </li>
                    <li class="list-inline-item">
                        <a href="" class="btn btn-rounded-icon">
                            <i class="ti-microsoft-alt"></i>
                            Nintendo
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>

<!--============================
=            Footer            =
=============================-->
<footer>
    <div class="footer-main">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-12 m-md-auto align-self-center">
                    <div class="block">
                        <ul class="social-icon list-inline">
                            <li class="list-inline-item">
                                <a href="https://www.facebook.com/"><i class="fab fa-facebook"></i></a>
                            </li>
                            <li class="list-inline-item">
                                <a href="https://twitter.com/"><i class="fab fa-twitter"></i></a>
                            </li>
                            <li class="list-inline-item">
                                <a href="https://www.instagram.com/"><i class="fab fa-instagram"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-2 col-md-3 col-6 mt-5 mt-lg-0">
                    <div class="block-2">
                        <!-- heading -->
                        <h6>Products</h6>
                        <!-- links -->
                        <ul>
                            <li><a href="team.html">Teams</a></li>
                            <li><a href="blog.html">Blogs</a></li>
                            <li><a href="FAQ.html">FAQs</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-2 col-md-3 col-6 mt-5 mt-lg-0">
                    <div class="block-2">
                        <!-- heading -->
                        <h6>Sing In/Up</h6>
                        <!-- links -->
                        <ul>
                            <li><a href="#">Singup</a></li>
                            <li><a href="#">Login</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-2 col-md-3 col-6 mt-5 mt-lg-0">
                    <div class="block-2">
                        <!-- heading -->
                        <h6>Company</h6>
                        <!-- links -->
                        <ul>
                            <li><a href="#">Career</a></li>
                            <li><a href="#">Contact</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-2 col-md-3 col-6 mt-5 mt-lg-0">
                    <div class="block-2">
                        <!-- heading -->
                        <h6>Company</h6>
                        <!-- links -->
                        <ul>
                            <li><a href="#">About</a></li>
                            <li><a href="#">Contact</a></li>
                            <li><a href="#">Team</a></li>
                            <li><a href="#">Privacy Policy</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="text-center bg-dark py-4">
        <small class="text-secondary">Copyright &copy; <script>document.write(new Date().getFullYear())</script>. Designed &amp; Developed by <a href="#">Jon</a></small class="text-secondary">
    </div>

    <div class="text-center bg-dark py-1">
        <small> <p>Distributed By <a href="#">My Self</a></p></small class="text-secondary">
    </div>
</footer>


<!-- To Top -->
<div class="scroll-top-to">
    <i class="ti-angle-up"></i>
</div>

<!-- JAVASCRIPTS -->

<script src="{{asset('js/index.js')}}"></script>
@if ($message = Session::get('success'))
<script>
    window.alert(String('{{$message}}'));
    localStorage.clear();
    counterItemsCart();
</script>
@endif
</body>

</html>
